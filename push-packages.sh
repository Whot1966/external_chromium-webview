#!/usr/bin/env bash

WHOT1966_CHROMIUM_VERSION=113.0.5672.132

git add patches/ Android.mk CleanSpec.mk README push-packages.sh
git commit -m "Import Chromium Webview patches to $WHOT1966_CHROMIUM_VERSION"
git push
git add prebuilt/arm
git commit -m "Import Chromium Webview arm prebuilt to $WHOT1966_CHROMIUM_VERSION"
git push
git add prebuilt/arm64
git commit -m "Import Chromium Webview arm64 prebuilt to $WHOT1966_CHROMIUM_VERSION"
git push
git add prebuilt/x86
git commit -m "Import Chromium Webview x64 prebuilt to $WHOT1966_CHROMIUM_VERSION"
git push
git add prebuilt/x86_64
git commit -m "Import Chromium Webview x86_64 prebuilt to $WHOT1966_CHROMIUM_VERSION"
git push
